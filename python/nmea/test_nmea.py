import serial
import pynmea2

with serial.Serial('/dev/ttyUSB0', baudrate=4800, timeout=1) as ser:
	# read 10 lines from the serial output
	for i in range(10):
		line = ser.readline().decode('ascii', errors='replace')
		#print(line.strip())

		nmeaobj = pynmea2.parse(line)
		['%s: %s' % (nmeaobj.fields[i][0], nmeaobj.data[i])      
			for i in range(len(nmeaobj.fields))]
