package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

var (
	// Variable globale de stockage de la dernière température lue
	temperature float32
	// Variable d'accès au composant de la sonde via I2C
	//htu21d *sensors.HTU21D

	wg         sync.WaitGroup
	finLecture chan bool
)

func main() {
	// Initialisation de la variable pour transmettre la fin de lecture
	finLecture = make(chan bool, 1)

	// Création d’une variable pour l’interception du signal de fin de programme
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, syscall.SIGTERM)
	signal.Notify(c, syscall.SIGKILL)
	// Go routine (thread parallèle) d’attente de fin du programme
	go func() {
		<-c

		// Notification de fin de lecture pour quitter la focntion litSonde()
		finLecture <- true
		// Attend que le compteur de groupe soit à 0
		wg.Wait()

		//htu21d.Close()

		os.Exit(0)
	}()

	var err error
	//htu21d, err = sensors.NewHTU21D(sensors.HTU21D_ADDR, 1)
	if err != nil {
		log.Println("Erreur d'initialisation du composant HTU21D")
		return
	}

	// Incrémente de 1 le compteur de groupe
	wg.Add(1)
	// Go routine (thread parallèle) de lecture de la sonde
	go litSonde()

	// Démarrage du serveur Web
	http.HandleFunc("/", handler)
	http.ListenAndServe(":8080", nil)
}

func litSonde() {
	// Instructions exécutées lorsque la fonction sera quitter
	defer func() {
		// Décrémente de 1 le compteur de groupe
		wg.Done()
	}()

	for {
		select {
		case <-finLecture:
			// Fin de lecture demandée, quitter la fonction
			return
		default:
			// Lecture de la température
			/*
			   			if temp, err := htu21d.ReadTemperature(); err == nil {
			   				// Si aucune erreur, mémoriser la valeur
			   				temperature = temp
			               }
			*/
			temperature += 1.0
		}

		// Attente d’une seconde
		time.Sleep(time.Second)
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "<div style='text-align: center'><p>Température</p><h1>%0.2f° C</h1></div>", temperature)
}
