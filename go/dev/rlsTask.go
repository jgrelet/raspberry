package main

import (
	"fmt"
	"strings"
)

// RlsData OTT RLS Radar give a distance
type RlsData struct {
	LengthInMeters *float64 `json:"meters"`
}

// readRls used as goroutine
func readRls() {

	if strings.Contains(strings.ToLower(simulDevice), sounder) {
		go simulRls()
		for {
			data := <-simulRlsChan
			switch sentence := data.(type) {
			case string:
				analyseSentence(sentence)
			}
		}
	} else {
		// get and display information about available port
		serialGetInfo()

		// get port for the Echo-sounder
		port := openSerialPort(cfg.Rls)

		for {
			sentence, err := readNmeaSentence(port)
			if err != nil {
				break
			}
			analyseSentence(sentence)
		}
	}
}

// String interface return GPSData string
func (d RlsData) String() string {

	var rv string
	if d.LengthInMeters != nil {
		rv += fmt.Sprintf("Length in meters: %7.2f\n", *d.LengthInMeters)
	}
	return strings.TrimSpace(rv)
}
