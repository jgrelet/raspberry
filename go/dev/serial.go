package main

import (
	"fmt"
	"log"
	_ "path/filepath"

	"go.bug.st/serial.v1"
)

func serialGetInfo() {
	// Retrieve the port list
	ports, err := serial.GetPortsList()
	if err != nil {
		log.Fatal(err)
	}
	if len(ports) == 0 {
		log.Fatal("No serial ports found!")
	}

	// Print the list of detected ports
	for _, port := range ports {
		fmt.Printf("Found port: %v\n", port)
	}
}

func openSerialPort(device SerialPort) serial.Port {
	// Open the first serial port detected at 9600bps N81
	mode := &serial.Mode{
		BaudRate: device.Baud,
		Parity:   serial.NoParity,
		DataBits: 8,
		StopBits: serial.OneStopBit,
	}
	// add test is cfg.Gps.Device == ports[n]
	fmt.Printf("Device found from %s : %v\n", cfgFile, device)
	fmt.Printf("Use port: %v\n", device.Device)
	port, err := serial.Open(device.Device, mode)
	if err != nil {
		fmt.Println(fmt.Errorf("Can't open serial port %s -> %s, exit", device.Device, err))
		log.Fatal(err)
	}
	return port
}

// readNmeaSentence read NMEA sentence from serial stream
func readNmeaSentence(port serial.Port) (response string, err error) {
	var n int
	buff := make([]byte, 10)
	var stringbuff string
	var state int
	var endOfSentence bool

	defer func() {
		if e := recover(); e != nil {
			fmt.Println(fmt.Errorf("serial port %T is disconnected -> %s, please check RS232 or USB connection", port, err))
		}
	}()

	for {
		n, err = port.Read(buff)
		if err != nil {
			fmt.Println(err)
			log.Panic(err)
			break
		}
		if n == 0 {
			fmt.Println(err)
			log.Fatal("\nEOF")
			return "", err
		}
		//fmt.Printf("%v\n", string(buff[:n]))
		switch string(buff[:n]) {
		case "\r":
			state = 1
			//fmt.Println("found carriage return")
		case "\n":
			endOfSentence = true
			if state == 1 {
				//fmt.Println("found newline")
				state = 2
			} else {
				state = 0
				log.Printf("Invalid end of line, CR is missing: \n%s", stringbuff)
				stringbuff = ""
			}
		default:
			if state == 1 {
				state = 0
				log.Printf("Invalid end of line, LF is missing: \n%s", stringbuff)
				stringbuff = string(buff[:n])
			} else {
				stringbuff = fmt.Sprintf("%s%s", stringbuff, string(buff[:n]))
			}
		}
		if endOfSentence && state == 2 {
			//fmt.Printf("result: %s\n", stringbuff)
			break
		}
	}
	return stringbuff, nil
}
