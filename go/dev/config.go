package main

import (
	"fmt"
	"log"

	"github.com/BurntSushi/toml"
)

// SerialPort structure for a serial port
type SerialPort struct {
	Type   string
	Use    bool
	Device string
	Baud   int
}

// Config is the Go representation of toml file
type Config struct {
	Global struct {
		Author string
		Debug  bool
		Echo   bool
		Log    string
	}
	Acq struct {
		File string
	}
	Gps         SerialPort
	EchoSounder SerialPort
	Rls         SerialPort
}

// GetConfig give the content of toml configFile
func (cfg *Config) GetConfig(configFile string) {

	//  read config file
	if _, err := toml.DecodeFile(configFile, &cfg); err != nil {
		log.Fatal(fmt.Sprintf("Error func GetConfig: file= %s -> %s\n", configFile, err))
	}
}
