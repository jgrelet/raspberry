package main

import (
	"io/ioutil"
	"log"
	"os"
	_ "path/filepath"
)

// use for echo mode
// Discard is an io.Writer on which all Write calls succeed
// without doing anything. Discard = devNull(0) = int(0)
// when echo is define in program argument list, echo = os.Stdout
var echo, debug = ioutil.Discard, ioutil.Discard

// logFile log error
// dataFile used to save all data in ASCII
// jsonFile used to save raw data
var logFile, dataFile, jsonFile *os.File

// Config retrieve parameters from toml files
var cfg Config

// the device display in terminal mode
var device, simulDevice string

// cfgFile mane  toml files
var cfgFile = "nmea.toml"

// channels  using for communication between tasks
var (
	gpsChan,
	simulGpsChan,
	simulEchoSounderChan,
	simulRlsChan chan interface{}
)

func init() {
	// get command line options
	GetOptions()
	// read configuration file, by default, optcfgFile = cfgname
	cfg.GetConfig(cfgFile)

	// open log, stdout or file
	if logFile != os.Stdout {
		logFile, _ = os.OpenFile(cfg.Global.Log, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
	}
	log.SetOutput(logFile)
	log.SetPrefix("\n[NMEA START]")
	log.Println("Acquisition Begin")
	//log.SetFlags(0)
	log.SetPrefix("[NMEA HS]")

	// open data files
	dataFile, _ = os.OpenFile(cfg.Acq.File, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
	jsonFile, _ = os.OpenFile("data.json", os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
}

// main entry
func main() {
	defer logFile.Close()
	defer dataFile.Close()

	// initialize channel communication for
	gpsChan = make(chan interface{})
	simulGpsChan = make(chan interface{})
	simulEchoSounderChan = make(chan interface{})

	// read GPS on serial port or simulation
	go readNMEA()
	go readEchoSounder()

	// main loop
	for {
		data := <-gpsChan
		switch v := data.(type) {
		case string:
			dataFile.WriteString(v)
		case []byte:
			jsonFile.Write(v)
			jsonFile.WriteString("\n")
		}
	}
}
