package main

import (
	"fmt"
	"math/rand"
	"time"
)

// const payloadRls = "0+%+0"

var length = 3.085

func simulRls() {
	tickChan := time.NewTicker(time.Millisecond * 2000).C

	for {
		select {
		case <-tickChan:

			// fmt.Printf("%v\n", s)
			simulRlsChan <- fmt.Sprintf("0+%5.3f+0", computeNextLength(length))
		}
	}
}

// from length, compute a new randon depth in meters, feet and fathoms
func computeNextLength(length float64) float64 {
	return (rand.Float64() * .05) + length
}
