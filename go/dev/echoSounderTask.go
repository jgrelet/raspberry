package main

import (
	"fmt"
	"strings"
)

// EchoSounderData defined nmea DBT sentence content
type EchoSounderData struct {
	DepthInFeet    *float64 `json:"feet"`    // ft
	DepthInMeters  *float64 `json:"meters"`  // mph
	DepthInFathoms *float64 `json:"fathoms"` // ft/min
}

// readGPS used as goroutine
func readEchoSounder() {

	if strings.Contains(strings.ToLower(simulDevice), sounder) {
		go simulEchoSounder()
		for {
			data := <-simulEchoSounderChan
			switch sentence := data.(type) {
			case string:
				analyseSentence(sentence)
			}
		}
	} else {
		// get and display information about available port
		serialGetInfo()

		// get port for the Echo-sounder
		port := openSerialPort(cfg.EchoSounder)

		for {
			sentence, err := readNmeaSentence(port)
			if err != nil {
				break
			}
			analyseSentence(sentence)
		}
	}
}

// String interface return GPSData string
func (d EchoSounderData) String() string {

	var rv string
	if d.DepthInMeters != nil {
		rv += fmt.Sprintf("Depth in meters: %7.2f\n", *d.DepthInMeters)
	}
	if d.DepthInFeet != nil {
		rv += fmt.Sprintf("Depth in feet: %7.2f\n", *d.DepthInFeet)
	}
	if d.DepthInFathoms != nil {
		rv += fmt.Sprintf("Depth in fathoms: %7.2f\n", *d.DepthInFathoms)
	}
	return strings.TrimSpace(rv)
}
