// GetOptions
package main

import (
	"fmt"
	"os"

	"github.com/pborman/getopt/v2"
)

var (
	// Version set in Makefile
	// see: http://stackoverflow.com/questions/11354518/golang-application-auto-build-versioning
	Version = "0.1"
	// Binary set in Makefile
	Binary = "dev"
	// BuildTime set in Makefile
	BuildTime = "undefined"
)

// global arg list options
//var optDebug *bool
//var optEcho *bool

// GetOptions parse arguments on command line and return files list to process
func GetOptions() {

	// parse options, moved outside main
	optHelp := getopt.BoolLong("help", 'h', "Help")
	optVersion := getopt.BoolLong("version", 'v', "Show version, then exit.")
	optEcho := getopt.BoolLong("echo", 'e', "Display processing in stdout")
	optDebug := getopt.BoolLong("debug", 'd', "Display debug info")
	optLog := getopt.BoolLong("log", 'l', "Write log, defaut is true")
	optTerminal := getopt.StringLong("trace", 't', device, "Display terminal for: GPS, Echo-sounder or Radar")
	optSimul := getopt.StringLong("simul", 's', simulDevice, "Simulate: GPS, Echo-sounder or Radar")
	optcfgFile := getopt.StringLong("config", 'c', cfgFile, "use specific configuration .toml file", "nmea.toml")

	// parse options line argument
	getopt.Parse()

	// process bloc when option is set
	if *optHelp {
		getopt.Usage()
		os.Exit(0)
	}
	// equal to : dev -s gps -e -t gps -l
	if *optDebug {
		debug = os.Stdout
		echo = os.Stdout
		logFile = os.Stdout
		simulDevice = gps + "," + sounder
		//simulDevice = gps
		device = gps + "," + sounder
		//device = gps
	}
	if *optEcho {
		echo = os.Stdout
	}
	if *optLog {
		logFile = os.Stdout
	}
	if *optTerminal != "" {
		device = *optTerminal
	}
	if *optSimul != "" {
		simulDevice = *optSimul
	}
	if *optcfgFile != "" {
		cfgFile = *optcfgFile
	}
	// show version and env
	if *optVersion {
		fmt.Println(Binary + ": v" + Version + ", build date: " + BuildTime)
		fmt.Printf("Configuration file: %s\n", cfgFile)
		fmt.Printf("GOPATH: %s\n", os.Getenv("GOPATH"))
		fmt.Printf("GOBIN: %s\n", os.Getenv("GOBIN"))
		os.Exit(0)
	}
}
