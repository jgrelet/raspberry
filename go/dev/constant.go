package main

import (
	"fmt"
	"math"

	nmea "github.com/jgrelet/go-nmea"
)

// usefull shortcut macros
var p = fmt.Println
var f = fmt.Printf

var badLatLong = nmea.LatLong(1e+36)

const (
	// Allowed devices names
	gps     = "gps"
	sounder = "sounder"
	radar   = "radar"
)

const (
	radToDeg  = 180 / math.Pi
	degToRad  = math.Pi / 180
	radToGrad = 200 / math.Pi
	gradToDeg = math.Pi / 200
)

const (
	kmToMile = 1. / 1.852
	mileToKm = 1. * 1.852
)
