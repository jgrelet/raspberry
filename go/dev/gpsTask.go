package main

import (
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	nmea "github.com/jgrelet/go-nmea"
)

// GPSData defined nmea sentence content
type GPSData struct {
	LastUpdate time.Time `json:"last_update"`

	AntennaStatus *string `json:"ant_status"`

	Status           *nmea.FixStatus        `json:"status"`
	QualityIndicator *nmea.QualityIndicator `json:"Quality"`

	Latitude  *nmea.LatLong `json:"latitude"`
	Longitude *nmea.LatLong `json:"longitude"`
	Altitude  *uint         `json:"altitude"` // ft
	Speed     *uint         `json:"speed"`    // mph
	Climb     *uint         `json:"climb"`    // ft/min

	LatitudeAccuracyErr  *uint `json:"latitude_accuracy_err"`
	LongitudeAccuracyErr *uint `json:"longitude_accuracy_err"`
	AltitudeAccuracyErr  *uint `json:"altitude_accuracy_err"`
	SpeedAccuracyErr     *uint `json:"speed_accuracy_err"`
	CourseAccuracyErr    *uint `json:"course_accuracy_err"`
}

// analyseSentence decode Nmea sentence
// todos: add a list og talker, eg: GGA, GSV, etc
func analyseSentence(sentence string) {

	// initialize structure GPSData
	var state GPSData
	// test if terminal mode is true for device
	// try io.Copy(os.Stdout, port)
	if strings.Contains(strings.ToLower(device), gps) {
		fmt.Println(sentence)
	}
	t := time.Now()
	msg, err := nmea.Parse(sentence)
	if err != nil {
		log.Println("Unable to decode nmea message, err:", err.Error())
		log.Printf("Invalid Sentence is: %v\n", sentence)
	}
	//fmt.Printf("%T,%v\n", msg, msg)
	switch msg.(type) {
	case *nmea.GPGGA:
		gpgga := msg.(*nmea.GPGGA)
		tmp := &gpgga.TimeUTC
		//fmt.Printf("%T, %v\n", gpgga.QualityIndicator, gpgga.QualityIndicator)
		if gpgga.QualityIndicator == nmea.InvalidIndicator {
			state.LastUpdate = tmp.AddDate(t.Year(), int(t.Month()-1), t.Day()-1)
			state.Latitude, state.Longitude = &badLatLong, &badLatLong
			state.QualityIndicator = &gpgga.QualityIndicator
		} else {
			state.LastUpdate, state.Latitude, state.Longitude, state.QualityIndicator =
				tmp.AddDate(t.Year(), int(t.Month()-1), t.Day()-1), &gpgga.Latitude, &gpgga.Longitude,
				&gpgga.QualityIndicator
				// display to screen
			fmt.Fprintln(echo, state)
			// the format write to data file, may be change, send to main channel OK or KO
			str := fmt.Sprintf("\n%s %10.6f %11.6f ", state.LastUpdate.Format("2006-01-02 15:04:05"),
				*state.Latitude, *state.Longitude)
			gpsChan <- str
		}

		data, _ := json.Marshal(gpgga)
		gpsChan <- data

	case *nmea.GPGSV:
		gpgsv := msg.(*nmea.GPGSV)
		data, _ := json.Marshal(gpgsv)
		gpsChan <- data
	case *nmea.GPDBT:
		var state EchoSounderData
		gpdbt := msg.(*nmea.GPDBT)
		state.DepthInMeters = &gpdbt.DepthInMeters
		//state.DepthInFeet = &gpdbt.DepthInFeet
		//state.DepthInFathoms = &gpdbt.DepthInFathoms
		fmt.Fprintln(echo, state)
		str := fmt.Sprintf("%7.2f", *state.DepthInMeters)
		gpsChan <- str
		data, _ := json.Marshal(gpdbt)
		gpsChan <- data
	}
}

// readGPS used as goroutine
func readNMEA() {

	if strings.Contains(strings.ToLower(simulDevice), gps) {
		go simulGps()
		for {
			data := <-simulGpsChan
			switch sentence := data.(type) {
			case string:
				analyseSentence(sentence)
			}
		}
	} else {
		// get and display information about available port
		serialGetInfo()

		// get port pour the GPS
		port := openSerialPort(cfg.Gps)

		for {
			sentence, err := readNmeaSentence(port)
			if err != nil {
				break
			}
			analyseSentence(sentence)
		}
	}
}

// String interface return GPSData string
func (d GPSData) String() string {

	rv := fmt.Sprintf("Time: %s\n", d.LastUpdate.Format("2006/01/02 15:04:05Z"))

	if d.AntennaStatus != nil {
		rv += fmt.Sprintf("Antenna status: %v\n", *d.AntennaStatus)
	}
	if d.Status != nil {
		rv += fmt.Sprintf("Status: %s\n", d.Status.String())
	}
	if d.QualityIndicator != nil {
		rv += fmt.Sprintf("Quality: %s\n", d.QualityIndicator.String())
	}
	if d.Latitude != nil {
		rv += fmt.Sprintf("Latitude: %s\n", d.Latitude.PrintDMS())
	}
	if d.Longitude != nil {
		rv += fmt.Sprintf("Longitude: %s\n", d.Longitude.PrintDMS())
	}
	if d.Altitude != nil {
		rv += fmt.Sprintf("Altitude: %v\n", *d.Altitude)
	}
	if d.Speed != nil {
		rv += fmt.Sprintf("Speed: %v\n", *d.Speed)
	}
	if d.Climb != nil {
		rv += fmt.Sprintf("Climb: %v\n", *d.Climb)
	}
	if d.LatitudeAccuracyErr != nil {
		rv += fmt.Sprintf("Latitude Err: %v\n", *d.LatitudeAccuracyErr)
	}
	if d.LongitudeAccuracyErr != nil {
		rv += fmt.Sprintf("Longitude Err: %v\n", *d.LongitudeAccuracyErr)
	}
	if d.AltitudeAccuracyErr != nil {
		rv += fmt.Sprintf("Altitude Err: %v\n", *d.AltitudeAccuracyErr)
	}
	if d.SpeedAccuracyErr != nil {
		rv += fmt.Sprintf("Speed Err: %v\n", *d.SpeedAccuracyErr)
	}
	if d.CourseAccuracyErr != nil {
		rv += fmt.Sprintf("Course Err: %v\n", *d.CourseAccuracyErr)
	}
	return strings.TrimSpace(rv)
}
