package main

import (
	"fmt"
	"time"
)

func main() {
	tickChan := time.NewTicker(time.Second * 1).C

	for {
		select {
		case <-tickChan:
			fmt.Println("Ticker ticked: ", time.Now())
		}
	}
}
