package main

import (
	"fmt"
	"github.com/adrianmo/go-nmea"
	"github.com/tarm/serial"
"log"
)

func main() {
	c := &serial.Config{Name: "/dev/ttyUSB0", Baud: 4800}
	s, err := serial.OpenPort(c)
	if err != nil {
		log.Fatal(err)
	}
	buf := make([]byte, 128)
	n, err := s.Read(buf)
	if err != nil {
		log.Fatal(err)
	}
	m, err := nmea.Parse(string(n))
	if err == nil {
		fmt.Printf("%+v\n", m)
	}
}
