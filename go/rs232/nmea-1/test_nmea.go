package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	_ "path/filepath"
	"strings"
	"time"

	nmea "github.com/pilebones/go-nmea"
	"github.com/tarm/serial"
)

//var device = flag.String("device", "/dev/ttyUSB0", "Serial port device to use, ex: COM1 or /dev/ttyUSB0")
var device = flag.String("device", "COM18", "Serial port device to use, ex: COM1 or /dev/ttyUSB0")
var baudrate = flag.Int("baud", 4800, "Baud rate to use")

//var datation = flag.Bool("d", false, "display date before datagram")

// GPSData defined nmea trame content
type GPSData struct {
	LastUpdate time.Time `json:"last_update"`
	GpsTime    time.Time

	AntennaStatus *string `json:"ant_status"`

	Status *nmea.FixStatus `json:"status"`

	Latitude  *nmea.LatLong `json:"latitude"`
	Longitude *nmea.LatLong `json:"longitude"`
	Altitude  *uint         `json:"altitude"` // ft
	Speed     *uint         `json:"speed"`    // mph
	Climb     *uint         `json:"climb"`    // ft/min

	LatitudeAccuracyErr  *uint `json:"latitude_accuracy_err"`
	LongitudeAccuracyErr *uint `json:"longitude_accuracy_err"`
	AltitudeAccuracyErr  *uint `json:"altitude_accuracy_err"`
	SpeedAccuracyErr     *uint `json:"speed_accuracy_err"`
	CourseAccuracyErr    *uint `json:"course_accuracy_err"`
}

var (
	state GPSData
)

// Usage function call with -help arg
var Usage = func() {
	fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
	flag.PrintDefaults()
}

func readline(port *serial.Port) nmea.NMEA {
	raw := bufio.NewReader(port)
	reply, _, err := raw.ReadLine()
	if err != nil {
		panic(err)
	}
	msg, err := nmea.Parse(string(reply))
	if err != nil {
		fmt.Println("Unable to decode nmea message, err:", err.Error())
		return nil
	}
	//fmt.Println(msg.GetMessage())
	return msg
}

func (d GPSData) String() string {

	rv := fmt.Sprintf("Time: %s\n", d.LastUpdate.Format("2006-01-02 15:04:05"))

	if d.AntennaStatus != nil {
		rv += fmt.Sprintf("Antenna status: %v\n", *d.AntennaStatus)
	}

	if d.Status != nil {
		rv += fmt.Sprintf("Status: %s\n", d.Status.String())
	}

	if d.Latitude != nil {
		rv += fmt.Sprintf("Latitude: %v\n", *d.Latitude)
	}

	if d.Longitude != nil {
		rv += fmt.Sprintf("Longitude: %v\n", *d.Longitude)
	}

	if d.Altitude != nil {
		rv += fmt.Sprintf("Altitude: %v\n", *d.Altitude)
	}

	if d.Speed != nil {
		rv += fmt.Sprintf("Speed: %v\n", *d.Speed)
	}

	if d.Climb != nil {
		rv += fmt.Sprintf("Climb: %v\n", *d.Climb)
	}

	if d.LatitudeAccuracyErr != nil {
		rv += fmt.Sprintf("Latitude Err: %v\n", *d.LatitudeAccuracyErr)
	}

	if d.LongitudeAccuracyErr != nil {
		rv += fmt.Sprintf("Longitude Err: %v\n", *d.LongitudeAccuracyErr)
	}

	if d.AltitudeAccuracyErr != nil {
		rv += fmt.Sprintf("Altitude Err: %v\n", *d.AltitudeAccuracyErr)
	}

	if d.SpeedAccuracyErr != nil {
		rv += fmt.Sprintf("Speed Err: %v\n", *d.SpeedAccuracyErr)
	}

	if d.CourseAccuracyErr != nil {
		rv += fmt.Sprintf("Course Err: %v\n", *d.CourseAccuracyErr)
	}

	return strings.TrimSpace(rv)
}

func main() {
	flag.Parse()
	cfg := &serial.Config{Name: *device, Baud: *baudrate, ReadTimeout: time.Second * 5}
	sp, err := serial.OpenPort(cfg)
	if err != nil {
		log.Fatalf("Open failed: %s: %s\n", err, *device)
	}

	for {
		msg := readline(sp)
		//fmt.Println(msg)
		state.LastUpdate = time.Now()
		fmt.Printf("Type: %T\n", msg)
		switch msg.(type) {
		case *nmea.GPGGA:
			gpgga := msg.(*nmea.GPGGA)
			state.LastUpdate, state.Latitude, state.Longitude = gpgga.TimeUTC, &gpgga.Latitude, &gpgga.Longitude
			fmt.Println("Decoded: ", state)
		}
	}
}
