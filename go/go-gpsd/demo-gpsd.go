package main

import "fmt"
import "github.com/stratoberry/go-gpsd"

func main() {
	var gps *gpsd.Session
	var err error

	var defaultAddress = "134.246.159.167:2947"
	if gps, err = gpsd.Dial(defaultAddress); err != nil {
		panic(fmt.Sprintf("Failed to connect to GPSD: %s", err))
	}

	gps.AddFilter("TPV", func(r interface{}) {
		tpv := r.(*gpsd.TPVReport)
		fmt.Println("TPV", tpv.Mode, tpv.Time, tpv.Lat, tpv.Lon, tpv.Speed)
	})
	/*
	   skyfilter := func(r interface{}) {
	       sky := r.(*gpsd.SKYReport)

	       fmt.Println("SKY", len(sky.Satellites), "satellites")
	   }

	   gps.AddFilter("SKY", skyfilter)
	*/
	done := gps.Watch()
	<-done
}
var 